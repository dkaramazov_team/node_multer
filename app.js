const express = require('express');
const multer = require('multer');
const ejs = require('ejs');
const path = require('path');

// set storage engine
const storage = multer.diskStorage({
    destination: './public/uploads/',
    filename: function(req, file, cb){
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
});

const upload = multer({
    storage: storage,
    limits: { fileSize: 1000000 },
    fileFilter: function(req, file, cb){
        checkFileType(file, cb);
    }
}).single('myImage');

function checkFileType(file, cb){
    const fileTypes = /jpeg|jpg|png|gif/;
    const extName = fileTypes.test(path.extname((file.originalname).toLowerCase()));
    const mimeType = fileTypes.test(file.mimetype);
    return mimeType && extName ? cb(null, true): cb('Error: images only!', false);
}

const app = express();

app.set('view engine', 'ejs');

app.use(express.static('./public'));

app.get('/', (req, res) => {
     res.render('index')
});

app.post('/upload', (req, res) => {
    upload(req, res, (err) => {
        if(err){
            res.render('index', {
                msg: err
            });
        } else{
            if(req.file == undefined){
                res.render('index', {
                    msg: 'Error: no file selected'
                });
            } else {
                res.render('index', {
                    msg: 'File uploaded!',
                    file: `uploads/${req.file.filename}`
                })
            }
        }
    });
});

const port = process.env.PORT || 3000;

app.listen(port, () => {
    console.log(`server started on port ${port}`);
});

